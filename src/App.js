import React from 'react';
import TestComponent from './components/TaskOne/TaskOne'

function App() {
  return (
    <div className="App">
      <header className="App-header">

        <TestComponent />
      </header>
    </div>
  );
}

export default App;
