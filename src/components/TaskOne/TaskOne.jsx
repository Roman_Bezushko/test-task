import React, {Component} from 'react';
import TaskTwo from "../TaskTwo/TaskTwo";
import TaskThree from "../TaskThree/TaskThree";
import './TaskOne.scss'

class TestComponent extends Component {
  state={
    name: '',
    email: '',
    password: ''
  };

  handleChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value
    })
  }

  submitLoginForm = (e) => {
    const {name, email, password} = this.state;

    e.preventDefault()
    console.log('SUBMITTED')
    console.log(name, email, password)

    this.refs.simpelCheckbox.checked = false
    this.setState({
      name: '',
      email: '',
      password: ''
    })
  }


  render() {
    return (
      <div className="main-wrapper">
        <div className="form-wrapper">
          <h2>TASK ONE: Simple form</h2>
          <form onSubmit={this.submitLoginForm}>
            <label htmlFor="name">Name:
              <input className="input-field" type="text" name="name" value={this.state.name} onChange={this.handleChange} required />
            </label>
            <label htmlFor="email">Email:
              <input className="input-field" type="email" name="email" value={this.state.email} onChange={this.handleChange} required />
            </label>
            <label htmlFor="password">Password:
              <input
                className="input-field"
                type="password"
                name="password"
                value={this.state.password}
                onChange={this.handleChange}
                required
                minLength="6"
                maxLength="12"
              />
            </label>
            <label>
              <input
                className="input-field"
                type="checkbox"
                ref="simpelCheckbox" required
              /> SOME CHECKBOX
            </label>

            <button
              className="submit-button"
              type="submit">
              Login
            </button>

          </form>


          <br/>
          <br/>
          <br/>
          <TaskTwo />


          <br/>
          <br/>
          <br/>

          <TaskThree />
        </div>
      </div>
    );
  }
}

TestComponent.propTypes = {};

export default TestComponent;
