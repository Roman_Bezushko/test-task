import React, {Component} from 'react';
import axios from 'axios';
import PropTypes from 'prop-types';

class TaskThree extends Component {
  state = {
    data: []
  };
  componentDidMount() {
    axios.get('https://api.jsonbin.io/b/5b97f370db948c68635f6dbc')
      .then(response => {
        this.setState({
          data: response.data.data
        })
      })
  }
  render() {
    const { data } = this.state;
    return (
      <div>
        <h2>Task Three</h2>
        <ul>
          {data.map(elem => (
            <li key={elem.id}>{elem.name}</li>
          ))}
        </ul>

      </div>
    );
  }
}

TaskThree.propTypes = {
  data: PropTypes.array,
};

export default TaskThree;
