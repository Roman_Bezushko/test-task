import React, {Component} from 'react';
import './TaskTwo.scss';

class TaskTwo extends Component {
  state={
    newTask: '',
    toDoList: [],
    doneList: []
  }

  componentDidMount() {
    if (localStorage.getItem('toDoList') || localStorage.getItem('doneList')) {
      const toDoList = JSON.parse(localStorage.getItem('toDoList'));
      const doneList = JSON.parse(localStorage.getItem('doneList'));

      this.setState({
        toDoList: toDoList ? toDoList : [],
        doneList: doneList ? doneList : [],
      })
    }
  }

  handleTaskChange =(e) => {
    this.setState({newTask: e.target.value})
  };

  handleAddNewTask =() => {

    this.setState(prevState => ({
      toDoList: [...prevState.toDoList, this.state.newTask],
      newTask: '',
    }))
    const toDoList = [...this.state.toDoList, this.state.newTask]
    localStorage.setItem('toDoList', JSON.stringify(toDoList))
  };

  handleIntoDoneList = (elem) => {
    this.setState(prevState => ({
      doneList: [...prevState.doneList, elem],
      toDoList: prevState.toDoList.filter(elemTodo => elemTodo !== elem)
    }));

    const toDoList = this.state.toDoList.filter(elemTodo => elemTodo !== elem)
    const doneList = [...this.state.doneList, elem]
    localStorage.setItem('doneList', JSON.stringify(doneList))
    localStorage.setItem('toDoList', JSON.stringify(toDoList))
  };

  render() {
    const {toDoList, doneList} = this.state;
    return (
      <div >
        <h2>Task Two</h2>
        <label htmlFor="addNewTask">Add new task:
          <input name="addNewTask" type="text" value={this.state.newTask} onChange={this.handleTaskChange} />
          <button onClick={this.handleAddNewTask}>Add New Task</button>
        </label>

        <div className="columns-wrapper">
          <div className="todo">
            <h2>TO DO</h2>
            {toDoList.map((elem, index) => (
              <div key={elem+index}>
                <p className="list-element" onClick={() => this.handleIntoDoneList(elem)}>{elem}</p>
              </div>
            ))}
          </div>

          <div className="done">
            <h2>Done</h2>
            {doneList.map((elem, index) => (
              <div  key={elem+index}>
                <p className="list-element green">{elem}</p>
              </div>
            ))}
          </div>
        </div>


      </div>
    );
  }
}

TaskTwo.propTypes = {};

export default TaskTwo;
